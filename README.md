# recipe-app-api-proxy

NGINX proxy app for our reciper app APO

## Usage

### Environment varaiables

* 'LISTEN_PORT' - Port to listen on (default: '8000')
* 'APP_HOST' - Hostname of the app to forward requests to (default: 'app')
* 'APP_PORT' - Port of the app to forward requests to (default: '9000')